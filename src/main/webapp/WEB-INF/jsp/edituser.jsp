<%@page import="com.hitekdada.kulish.andrey.uianddbforuniversity.dto.UserDao" %>
<jsp:useBean id="u" class="com.hitekdada.kulish.andrey.uianddbforuniversity.dto.entity.User"/>
<jsp:setProperty property="*" name="u"/>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit User</title>
</head>
<body>
<c:if test="${editUserSuccess}">
    <div>Successfully edit user with ID: ${idEdited}</div>
</c:if>

<c:url var="edit_user_url" value="/edituser"/>
<c:if test="${!editUserSuccess}">
    <form:form action="${edit_user_url}" method="post" modelAttribute="user">
        <form:input type="text" path="id"/> <form:label path="id">Id: </form:label> <BR>
        <form:input type="text" path="name"/> <form:label path="name">Name: </form:label> <BR>
        <form:input type="text" path="password"/> <form:label path="password">Password: </form:label> <BR>
        <form:input type="text" path="email"/><form:label path="email">Email: </form:label> <BR>
        <form:input type="text" path="sex"/> <form:label path="sex">Sex: </form:label> <BR>
        <form:input type="text" path="country"/> <form:label path="country">Country: </form:label> <BR>
        <input type="submit" value="submit"/>
    </form:form>
</c:if>
<a href="/viewusers">View Users</a>
</body>
</html>