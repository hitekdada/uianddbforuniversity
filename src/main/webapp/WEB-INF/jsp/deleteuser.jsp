<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete User</title>
</head>
<body>
<c:if test="${deleteUserSuccess}">
    <div>Deleted user with ID: ${id}</div>
</c:if>

<a href="/viewusers">View Users</a>
</body>
</html>