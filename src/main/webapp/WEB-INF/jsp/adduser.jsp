<%@page import="com.hitekdada.kulish.andrey.uianddbforuniversity.dto.UserDao" %>
<jsp:useBean id="u" class="com.hitekdada.kulish.andrey.uianddbforuniversity.dto.entity.User"/>
<jsp:setProperty property="*" name="u"/>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add User</title>
</head>
<body>
<c:if test="${addUserSuccess}">
    <div>Successfully added user with ID: ${savedUser.id}</div>
</c:if>

<c:url var="add_user_url" value="/adduser"/>
<form:form action="${add_user_url}" method="post" modelAttribute="user">
    <form:input type="text" path="name"/> <form:label path="name">Name: </form:label> <BR>
    <form:input type="text" path="password"/> <form:label path="password">Password: </form:label> <BR>
    <form:input type="text" path="email"/><form:label path="email">Email: </form:label> <BR>
    <form:input type="text" path="sex"/> <form:label path="sex">Sex: </form:label> <BR>
    <form:input type="text" path="country"/> <form:label path="country">Country: </form:label> <BR>
    <input type="submit" value="submit"/>
</form:form>
<a href="/viewusers">View Users</a>
</body>
</html>