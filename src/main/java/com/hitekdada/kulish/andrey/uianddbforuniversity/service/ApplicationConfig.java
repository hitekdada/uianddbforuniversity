package com.hitekdada.kulish.andrey.uianddbforuniversity.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.embedded.ConnectionProperties;
import org.springframework.jdbc.datasource.embedded.DataSourceFactory;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.Driver;

@Configuration
@EnableJpaRepositories("com.hitekdada.kulish.andrey.uianddbforuniversity.repository")
@EnableTransactionManagement
@PropertySource("classpath:application.yml")
public class ApplicationConfig {

    @Autowired
    private Environment env;

    @Bean
    public DataSourceFactory embeddedDataSourceFactory() {
        return new DataSourceFactory() {
            @Override
            public ConnectionProperties getConnectionProperties() {
                return new ConnectionProperties() {

                    @Override
                    public void setUsername(String username) {
                    }

                    @Override
                    public void setPassword(String password) {
                    }

                    @Override
                    public void setUrl(String url) {
                    }

                    @Override
                    public void setDriverClass(Class<? extends Driver> driverClass) {
                    }

                };
            }

            @Override
            public DataSource getDataSource() {
                SimpleDriverDataSource sds = new SimpleDriverDataSource();
                sds.setDriverClass(org.h2.Driver.class); // <- try to return Driver object too (using setDriver)
                sds.setUrl(env.getProperty("spring.datasource.url"));
                sds.setUsername("sa");
                sds.setPassword("sa");
                return sds;
            }
        };
    }

    @Bean
    public DataSource dataSource(DataSourceFactory embeddedDataSourceFactory) {

        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .setName("db")
                .setScriptEncoding("UTF-8")
                .setDataSourceFactory(embeddedDataSourceFactory)
                .build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.hitekdada.kulish.andrey.uianddbforuniversity");
        factory.setDataSource(dataSource);
        return factory;
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {

        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory);
        return txManager;
    }

}
