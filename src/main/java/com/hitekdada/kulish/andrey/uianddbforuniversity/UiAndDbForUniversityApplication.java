package com.hitekdada.kulish.andrey.uianddbforuniversity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "com.hitekdada.kulish.andrey.uianddbforuniversity")
public class UiAndDbForUniversityApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(UiAndDbForUniversityApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(UiAndDbForUniversityApplication.class);
    }

}
