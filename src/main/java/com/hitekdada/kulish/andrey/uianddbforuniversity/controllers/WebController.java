package com.hitekdada.kulish.andrey.uianddbforuniversity.controllers;

import com.hitekdada.kulish.andrey.uianddbforuniversity.dto.UserDao;
import com.hitekdada.kulish.andrey.uianddbforuniversity.dto.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class WebController {
    private static final String SLASH = "/";
    private static final String INDEX_JSP = "index";
    private static final String ADD_USER = "adduser";
    private static final String DELETE_USER = "deleteuser";
    private static final String EDIT_USER = "edituser";
    private static final String VIEW_USERS = "viewusers";

    public WebController() {
    }

    @Autowired
    private UserDao userDao;

    @GetMapping({SLASH + INDEX_JSP})
    public ModelAndView index() {
        return new ModelAndView(INDEX_JSP);
    }

    @GetMapping({SLASH + ADD_USER})
    public String addUserView(Model model) {
        model.addAttribute("user", new User());
        return ADD_USER;
    }

    @PostMapping({SLASH + ADD_USER})
    public RedirectView addUser(@ModelAttribute("user") User user, RedirectAttributes redirectAttributes) {
        final RedirectView redirectView = new RedirectView("/adduser", true);
        userDao.save(user);
        redirectAttributes.addFlashAttribute("savedUser", user);
        redirectAttributes.addFlashAttribute("addUserSuccess", true);
        return redirectView;
    }


    @GetMapping({SLASH + DELETE_USER})
    public ModelAndView deleteUser(Integer id) {
        ModelAndView modelAndView = new ModelAndView(DELETE_USER);
        User user = new User();
        user.setId(id);
        userDao.delete(user);
        modelAndView.addObject("deleteUserSuccess", true);
        modelAndView.addObject("id", id);
        return modelAndView;
    }

    @GetMapping({SLASH + EDIT_USER})
    public String editUserView(Model model, Integer id) {
        if (id != null) {
            User user = userDao.getRecordById(id);
            model.addAttribute("user", user);
        } else {
            model.addAttribute("user", new User());
        }
        return EDIT_USER;
    }

    @PostMapping({SLASH + EDIT_USER})
    public RedirectView editUser(@ModelAttribute("user") User user, RedirectAttributes redirectAttributes) {
        final RedirectView redirectView = new RedirectView("/edituser", true);
        userDao.update(user);
        redirectAttributes.addFlashAttribute("idEdited", user.getId());
        redirectAttributes.addFlashAttribute("editUserSuccess", true);
        return redirectView;
    }

    @GetMapping({SLASH + VIEW_USERS})
    public ModelAndView viewUser() {
        ModelAndView modelAndView = new ModelAndView(VIEW_USERS);
        modelAndView.addObject("list", userDao.getAllRecords());
        return modelAndView;
    }

}
