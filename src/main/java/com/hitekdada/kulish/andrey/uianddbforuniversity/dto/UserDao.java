package com.hitekdada.kulish.andrey.uianddbforuniversity.dto;

import com.hitekdada.kulish.andrey.uianddbforuniversity.dto.entity.User;
import com.hitekdada.kulish.andrey.uianddbforuniversity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDao {

    @Autowired
    UserService userService;

    public int save(User u) {
        User user = userService.saveOrUser(u);
        return user.getId() != -1 ? 1 : 0;
    }

    public void update(User u) {
        userService.update(u);
    }

    public void delete(User u) {
        userService.deleteUser(u.getId());
    }

    public List<User> getAllRecords() {
        return userService.getAllUser();
    }

    public User getRecordById(int id) {
        return userService.getUserById(id);
    }
}
