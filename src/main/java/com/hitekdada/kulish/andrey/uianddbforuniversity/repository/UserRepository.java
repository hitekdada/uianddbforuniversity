package com.hitekdada.kulish.andrey.uianddbforuniversity.repository;

import com.hitekdada.kulish.andrey.uianddbforuniversity.dto.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>
{
}
