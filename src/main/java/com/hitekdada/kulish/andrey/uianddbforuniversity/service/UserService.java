package com.hitekdada.kulish.andrey.uianddbforuniversity.service;

import com.hitekdada.kulish.andrey.uianddbforuniversity.dto.entity.User;
import com.hitekdada.kulish.andrey.uianddbforuniversity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUser() {
        List<User> user = new ArrayList<>();
        userRepository.findAll().forEach(student -> user.add(student));
        return user;
    }

    public User getUserById(int id) {
        return userRepository.findById(id).get();
    }

    public User saveOrUser(User user) {
        return userRepository.save(user);
    }

    public User update(User user) {
        User userFromDb = userRepository.findById(user.getId()).get();
        userFromDb.setName(user.getName());
        userFromDb.setCountry(user.getCountry());
        userFromDb.setPassword(user.getPassword());
        userFromDb.setSex(user.getSex());
        userFromDb.setEmail(user.getEmail());
        return userRepository.save(userFromDb);
    }

    public void deleteUser(int id) {
        userRepository.deleteById(id);
    }
}
